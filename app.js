$(function () {
  //write your code here
  let btnRace = $(".btnRace"),
    btnStartOver = $(".btnStartOver"),
    car1 = $(".raceCar1"),
    car2 = $(".raceCar2"),
    time = $(".time"),
    race = $(".race"),
    finish = $(".finish"),
    CarWidth = $(".raceCar1").width(),
    raceTrack = $(window).width() - CarWidth,
    table1 = $(".table1"),
    table2 = $(".table2"),
    table3 = $(".table3"),
    place1 = "first",
    place2 = "second";

  let counter1 = 0;

  btnRace.on("click", function () {
    let counter = 3;

    let interval = setInterval(() => {
      if (counter > 0) {
        btnRace.prop("disabled", true);
        btnStartOver.prop("disabled", true);
        time.text(counter);
        counter--;

        race.animate({
          opacity: "0.5",
        });
      } else {
        let random1 = Math.floor(Math.random() * 6000);
        let random2 = Math.floor(Math.random() * 6000);
        counter1++;
        sessionStorage.setItem("counter1", counter1);

        race.animate({
          opacity: "1",
        });

        car2.animate(
          {
            left: raceTrack,
          },
          random2,
          function () {
            console.log(random2);
            if (random2 <= random1) {
              finish.animate({
                opacity: "1",
              });

              race.animate({
                opacity: "0.5",
              });
              table1.prepend(
                `<tr><td class="place1">Finished in: <span class="car1Color">${place1}</span> place with a time of: <span class="car1Color">${random2} </span> milliseconds! </td></tr>`
              );
              let data1 = place1;
              sessionStorage.setItem("savingData1", data1);
              sessionStorage.setItem("savingTime2", random2);
            } else {
              btnRace.prop("disabled", false);
              btnStartOver.prop("disabled", false);
              table1.prepend(
                `<tr><td class="place1">Finished in: <span class="car1Color">${place2}</span> place with a time of: <span class="car1Color">${random2} </span> milliseconds! </td></tr>`
              );
              let data1 = place2;
              sessionStorage.setItem("savingData1", data1);
              sessionStorage.setItem("savingTime2", random2);
            }
          }
        );

        car1.animate(
          {
            left: raceTrack,
          },
          random1,
          function () {
            console.log(random1);
            if (random1 <= random2) {
              finish.animate({
                opacity: "1",
              });
              race.animate({
                opacity: "0.5",
              });
              table2.prepend(
                `<tr><td class="place1">Finished in: <span class="car2Color">${place1}</span> place with a time of: <span class="car2Color">${random1} </span> milliseconds!</td></tr>`
              );
              let data2 = place1;
              sessionStorage.setItem("savingData2", data2);
              sessionStorage.setItem("savingTime1", random1);
            } else {
              btnRace.prop("disabled", false);
              btnStartOver.prop("disabled", false);
              table2.prepend(
                `<tr><td class="place1">Finished in: <span class="car2Color">${place2}</span> place with a time of: <span class="car2Color">${random1} </span> milliseconds! </td></tr>`
              );
              let data2 = place2;
              sessionStorage.setItem("savingData2", data2);
              sessionStorage.setItem("savingTime1", random1);
            }
          }
        );
        time.text("");
        clearInterval(interval);
      }
    }, 1000);
  });

  btnStartOver.on("click", function () {
    car1.animate({
      left: "0",
    });
    car2.animate({
      left: "0",
    });
    time.html("");
    race.animate({
      opacity: "1",
    });
    finish.animate({
      opacity: "0",
    });
  });

  let h3 = $("<h3>");

  function results() {
    let count = sessionStorage.getItem("counter1");
    if (count > 0) {
      h3.insertBefore(table3);
      h3.text("Results from the previous time you played this game:");
      let getData1 = sessionStorage.getItem("savingData1");
      let getData2 = sessionStorage.getItem("savingData2");
      let getTime2 = sessionStorage.getItem("savingTime2");
      let getTime1 = sessionStorage.getItem("savingTime1");
      let text1 = $(".car1Color").text();
      let text2 = $(".car2Color").text();
      table3.append(
        `<tr><td class="place1" ><span class="car1Color">${text1}</span> finished in <span class="car1Color">${getData1}</span> place, with a time of <span class="car1Color">${getTime2}</span> milliseconds!</td></tr><tr><td class="place1"><span class="car2Color">${text2}</span> finished in <span class="car2Color">${getData2}</span> place, with a time of <span class="car2Color">${getTime1}</span> milliseconds!</td></tr>`
      );
    }
  }
  window.onload = results();
});
